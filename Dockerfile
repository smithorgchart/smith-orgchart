FROM node:10-jessie
RUN yarn global add http-server
WORKDIR /app
EXPOSE 3000
ADD . .
CMD http-server -s -p 3000
