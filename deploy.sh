#!/usr/bin/env bash

ssh smith \
'cd smith-orgchart &&\
git pull &&\
docker-compose up -d --build'
