const getUrlParameter = function getUrlParameter (sParam) {
  let sPageURL = window.location.search.substring (1),
    sURLVariables = sPageURL.split ('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i ++) {
    sParameterName = sURLVariables[i].split ('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent (sParameterName[1]);
    }
  }
};

let data = [];
let orgChart = null;
init_ ()
$ (document).ready (() => {
  const user_id = getUrlParameter ('user_id')
  const brand = getUrlParameter ('brand')
  const URL = 'http://api.orgchart.smith.in.th/orgchart'

  axios.post (URL,
    {"user_id": `${user_id}`, "brand": brand}, {
      timeout: 300000,
    })
    .then (({data: d}) => {
      let nodes = d['data']['nodes'];
      data = d['data']['data'];
      $ ('#loading').hide ()
      $ ('#tree').show ()
      const toDelete = ['area', 'avatar', 'first_name', 'nick_name',
        'last_name', 'parent_id', 'phone']
      for (let i = 0; i < toDelete.length; i ++) {
        for (let j = 0; j < nodes.length; j ++) {
          delete nodes[j][toDelete[i]]
        }
      }

      const peopleElement = document.getElementById ("tree");
      orgChart = new getOrgChart (peopleElement, {
        theme: "myCustomTheme",
        orientation: getOrgChart.RO_TOP,
        enableEdit: false,
        enableDetailsView: false,
        scale: 0.5,
        enableGridView: true,
        primaryFields: ["name", "privilege_id", "seller_id"],
        photoFields: ["img"],
        parentIdField: "pid",
        idField: "id",
        dataSource: nodes,
        updatedEvent: () => clickHandler (),
        // clickNodeEvent: (sender, args) => {
        //   const {node} = args
        //   const n = _.find (data, d => d.id === node.id)
        //   const {brandurl, seller_id} = n
        //   const url = encodeURI (`${brandurl}/qr_register/${seller_id}`)
        //   console.log ("URL", url);
        //   brandurl ? window.open (url, '_blank') : null
        //   return false
        // },
      });

      clickHandler ();
    })
    .catch (err => {
        $ ('#loading').hide ()
        $ ('#error').show ()
        console.log ('POST ERR', err)
      },
    )
})


function getNodeByClickedBtn (el) {
  while (el.parentNode) {
    el = el.parentNode;
    if (el.getAttribute ("data-node-id"))
      return el;
  }
  return null;
}

function clickHandler () {
  let btns = document.getElementsByClassName ("btn");
  for (let i = 0; i < btns.length; i ++) {
    btns[i].addEventListener ("click", function () {
      let nodeElement = getNodeByClickedBtn (this);
      let action = this.getAttribute ("data-action");
      let id = nodeElement.getAttribute ("data-node-id");
      let node = orgChart.nodes[id];
      switch (action) {
        case "info":
          const {brandurl, seller_id} = node['data']
          const sid = seller_id.split (": ")[1].trim ()
          const url = encodeURI (`${brandurl}/qr_register/${sid}`)
          brandurl ? window.open (url, '_blank') : null
          break;
      }
    });
  }
}

function init_ () {
  $ ('#tree').hide ()
  getOrgChart.themes.myCustomTheme = Object.assign ({}, getOrgChart.themes.ula);
  getOrgChart.themes.myCustomTheme = {
    size: [500, 220],
    toolbarHeight: 46,
    textPoints: [
      {x: 10, y: 200, width: 500},
      {x: 210, y: 40, width: 290},
      {x: 210, y: 80, width: 290},
      {x: 210, y: 115, width: 290},
      {x: 210, y: 140, width: 290}],
    textPointsNoImage: [
      {x: 10, y: 200, width: 490},
      {x: 10, y: 40, width: 490},
      {x: 10, y: 65, width: 490},
      {x: 10, y: 90, width: 490},
      {x: 10, y: 115, width: 490},
      {x: 10, y: 140, width: 490}],
    expandCollapseBtnRadius: 20,
    box: '<rect x="0" y="0" height="220" width="500" rx="10" ry="10" class="get-box" />'
      + '<g transform="matrix(1,0,0,1,360,160)" class="btn" data-action="info"><rect width="120" height="40px" rx="5" ry=5></rect><text text-anchor="right" x="17" y="27" width="100">ข้อมูลตัวแทน</text></g>',
    text: '<text font-size="18" text-anchor="start" width="[width]" class="get-text get-text-[index]" x="[x]" y="[y]">[text]</text>',
    image: '<clipPath id="getMonicaClip"><circle cx="105" cy="65" r="85" /></clipPath><image preserveAspectRatio="xMidYMid slice" clip-path="url(#getMonicaClip)" xlink:href="[href]" x="20" y="-20" height="170" width="170"/>',
  }
}

// function setup () {
// const chart = new OrgChart(document.getElementById('tree'), {
//   template: "smith",
//   lazyLoading: true,
//   enableDragDrop: true,
//   showXScroll: BALKANGraph.scroll.visible,
//   showYScroll: BALKANGraph.scroll.visible,
//   mouseScroolBehaviour: BALKANGraph.action.zoom,
//   anim: {
//     func: BALKANGraph.anim.inOutPow,
//     duration: 1000,
//   },
//   onClick: (sender, node) => {
//     const n = _.find(data, d => d.id === node.id)
//     const {brandurl, seller_id} = n
//     const url=`${brandurl}/qr_register/${brand}?seller_id=${seller_id}`
//     console.log("URL", url);
//     brandurl ? window.open(url, '_blank') : null
//     return false
//   },
//   collapse: {
//     level: 3
//   },
//   nodeBinding: {
//     position: 'privilege_id',
//     name: 'name',
//     user_id: 'seller_id',
//     id: 'id',
//     pid: 'pid',
//     img_0: "img",
//   },
//   nodes: nodes,
// })
// OrgChart.templates.smith = Object.assign ({}, OrgChart.templates.ana);
// OrgChart.templates.smith.node = '<rect style="cursor:pointer" x="0" y="0" height="120" width="200" fill="#039BE5" stroke-width="1" stroke="#aeaeae" rx="5" ry="5"></rect>';
// OrgChart.templates.smith.size = [200, 120];
// OrgChart.templates.smith.plus = '<circle cx="15" cy="15" r="10" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle><line x1="8" y1="15" x2="22" y2="15" stroke-width="1" stroke="#aeaeae"></line><line x1="15" y1="8" x2="15" y2="22" stroke-width="1" stroke="#aeaeae"></line>'
// OrgChart.templates.smith.minus = '<circle cx="15" cy="15" r="10" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle><line x1="8" y1="15" x2="22" y2="15" stroke-width="1" stroke="#aeaeae"></line>'
// OrgChart.templates.smith.plus = '<g style="cursor:pointer;">'
//   + '<rect x="0" y="0" width="36" height="36" rx="12" ry="12" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></rect>'
//   + '<line x1="4" y1="18" x2="32" y2="18" stroke-width="1" stroke="#aeaeae"></line>'
//   + '<line x1="18" y1="4" x2="18" y2="32" stroke-width="1" stroke="#aeaeae"></line>'
//   + '</g>';
//
// OrgChart.templates.smith.minus = '<g style="cursor:pointer;">'
//   + '<rect x="0" y="0" width="36" height="36" rx="12" ry="12" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></rect>'
//   + '<line x1="4" y1="18" x2="32" y2="18" stroke-width="1" stroke="#aeaeae"></line>'
//   + '</g>';
//   OrgChart.templates.smith.position = '<text class="position" style="font-size: 13px;" fill="#ffffff" x="190" y="20" text-anchor="end">Position: {val}</text>';
//   OrgChart.templates.smith.user_id = '<text class="user_id" style="font-size: 12px;" fill="#ffffff" x="190" y="40" text-anchor="end">ID: {val}</text>';
//   OrgChart.templates.smith.name = '<text class="name" style="font-size: 14px;" fill="#ffffff" x="100" y="105" text-anchor="middle">{val}</text>';
// }
